# Formation Industrialisation d'un projet de data science avancée

Pour suivre ce TP nous allons utiliser les gitlab pages suivantes : 

[TP 0 Installation de l'environnement](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp0/index.html#0)

[TP 1 Mise en place de la CI](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp1/index.html#0)

[TP 2 Test behave](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp2/index.html#0)

[TP 3 Infra as code](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp3/index.html#0)

[TP 4 Orchestration](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp4/index.html#0)

[TP 5 Artefacts](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp5/index.html#0)

[TP 6 Model registry](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp6/index.html#0)

[TP 7 Exposition](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp7/index.html#0)

[TP 8 Monitoring](https://octo-technology.gitlab.io/octo-bda/cercle-formation/dsin2/tp8/index.html#0)

